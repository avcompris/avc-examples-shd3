#!/bin/sh

# File: avc-examples-shd3/docker/avcompris/examples-shd3/fetch_artifact.sh

set -e

VERSION_IN_DOCKERFILE=`grep VERSION Dockerfile | head -1 | awk '{ print $3 }'`

cp -R ../../../src/main/www .

# cp config.js www/js/

echo '{' > www/buildinfo.json

cat buildinfo | grep : | while read i; do
	name=`echo "${i}" | sed s/:.*//`
	value=`echo "${i}" | sed s/^[^:]*:\ *//`
	echo "    \"${name}\": \"${value}\"," >> www/buildinfo.json
done

echo "    \"version\": \"${VERSION_IN_DOCKERFILE}\"" >> www/buildinfo.json

echo '}' >> www/buildinfo.json
