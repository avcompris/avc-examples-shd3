// File: avc-examples-shd3/src/main/www/js/home.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			error: null,
		},
		methods: {

		},
		computed: {

		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE
});
