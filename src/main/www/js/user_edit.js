// File: avc-examples-shd3/src/main/www/js/user_edit.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

let user = null;
let error = null;

const username = extractField_from_hashFragment('username');

save_config(function() {

	$.get_object(get_API_BASE_URL('users') + '/' + username, function(json) {

		user = json;

	}).fail(function(xhr, status) {

		error = xhr.responseJSON;

		console.error(JSON.stringify(error));
	});
});

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null && user !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			error: error,
			user: user,
			role: user.role,
			password: null,
			enabled: user.enabled,
			roles: ROLES,
		},
		methods: {

			format_dateTimeHolder: global_format_dateTimeHolder,

			goto_user: function() {

				self.location = 'user_show.html#username=' + this.user.username + ',cancelled';
			},

			do_update_user: function() {

				$.put_object(get_API_BASE_URL('users') + '/' + app.user.username, {
					role: app.role,
					password: (app.password !== null && app.password.trim() !== '') ? app.password : null,
					enabled: app.enabled,
					fromRevision: app.user.revision,
				}, function(json) {

					// loadFrame('menuFrame').getApp().refresh_users();

					loadFrame('middleFrame', function(middleFrame) {

						if (middleFrame !== undefined) { middleFrame.getApp().do_query(); }

						self.location = 'user_show.html#username=' + app.user.username + ',updated';
					});

				}).fail(function(xhr, status) {

					app.error = xhr.responseJSON;

					console.error(JSON.stringify(app.error));
				});
			},
		},
		computed: {

			is_standalone: global_is_standalone,

			API_BASE_URL: global_API_BASE_URL,
		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

});
