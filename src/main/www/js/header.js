// File: avc-examples-shd3/src/main/www/js/header.js

// 1. INITIALIZE THE CONTEXT

// Define: app // From framework.js // Will be populated with the Vue object
// Load: pre_app.buildinfo // From framework.js

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			API_BASE_URL: 'https://examples3.avcompris.com/api/v1/',
			Authorization: 'xxx',
		},
		methods: {

			update_config: function() {

				save_config();

				loadFrame('leftFrame', function(leftFrame) {

					leftFrame.refresh_all();
				});
			},
		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

	// 3.1. COOKIES

	const API_BASE_URL = Cookies.get('API_BASE_URL');
	const Authorization = Cookies.get('Authorization');

	if (API_BASE_URL !== undefined) {
		app.API_BASE_URL = API_BASE_URL;
	}

	if (Authorization !== undefined) {
		app.Authorization = Authorization;
	}
});

function save_config(callback) {

	async_getApp().then(function() {

		Cookies.set('API_BASE_URL', app.API_BASE_URL);
		Cookies.set('Authorization', app.Authorization);

		if (callback) {
			callback();
		}
	});
}
