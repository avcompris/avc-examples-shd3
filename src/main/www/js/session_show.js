// File: avc-examples-shd3/src/main/www/js/session_show.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

let session = null;
let error = null;

const userSessionId = extractField_from_hashFragment('user_session_id');
const isCancelled = extractFlag_from_hashFragment('cancelled');
const isTerminated = extractFlag_from_hashFragment('terminated');

save_config(function() {

	$.get_object(get_API_BASE_URL('sessions') + '/' + userSessionId, function(json) {

		session = json;

	}).fail(function(xhr, status) {

		error = xhr.responseJSON;

		console.error(JSON.stringify(error));
	});
});

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null && session !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			error: error,
			session: session,
		},
		methods: {

			format_dateTimeHolder: global_format_dateTimeHolder,
		},
		computed: {

			is_standalone: global_is_standalone,

			API_BASE_URL: global_API_BASE_URL,
		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

	if (isCancelled) {
		flash_message('info', 'Action has been cancelled.');
	} else if (isTerminated) {
		flash_message('success', 'The session has been terminated.');
	}
});

function update_hash() {

	const userSessionId = extractField_from_hashFragment('user_session_id');

	save_config(function() {

		$.get_object(get_API_BASE_URL('sessions') + '/' + userSessionId, function(json) {

			session = json;

			if (app) { app.session = json; }

		}).fail(function(xhr, status) {

			error = xhr.responseJSON;

			if (error) {

				console.error(JSON.stringify(error));

			} else {

				console.error(xhr);
			}

			if (app) { app.error = xhr.responseJSON; }
		});
	});
}