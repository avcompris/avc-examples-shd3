// File: avc-examples-shd3/src/main/www/js/framework.js

let app = null;

// Global "pre_app" object with custom fields and custom methods.
//
const pre_app = {

	buildinfo: null,
};

const ROLES = [
	'REGULAR',
	'USERMGR',
	'SILENT_WORKER',
	'ADMIN',
];

function loadFrame(frameId, callback) {

	let timerId = setTimeout(function tick() {

		const frame = window.top[frameId];

		if (frame !== null) {

			clearTimeout(timerId);

			callback(frame);

		} else {

			timerId = setTimeout(tick, 100);
		}

	}, 100);
}

function save_config(callback) {

	if (window.top.topFrame !== undefined) {

		loadFrame('topFrame', function(topFrame) {

			topFrame.save_config(callback);
		});

	} else {

		callback();
	}
}

// This "getApp()" functions is for testability, since Selenium cannot read
// global variables such as "app", but can call global functions such as
// "getApp()"
//
function getApp() {

	return app;
}

async function async_getApp() {

	const promise = new Promise(function(resolve) {

		let timerId = setTimeout(function tick() {

			if (app !== null) {

				clearTimeout(timerId);

				resolve(app);

			} else {

				timerId = setTimeout(tick, 100);
			}

		}, 100);
	});

	return await promise;
}

function get_API_BASE_URL(label) {

	const API_BASE_URL = global_API_BASE_URL();

	if (/^http:\/\/localhost:8080/.test(API_BASE_URL)) {

		switch (label) {
			case 'users':
				return API_BASE_URL.replace('8080', '8080') + 'users';
			case 'sessions':
				return API_BASE_URL.replace('8080', '8080') + 'sessions';
			case 'logLines':
				return API_BASE_URL.replace('8080', '8780') + 'logging';
			default:
				console.error('get_API_BASE_URL(), unknown label: ' + label);
				return API_BASE_URL + label;
		}

	} else {

		switch (label) {
			case 'logLines':
				label = 'logging';
				break;
			default:
				break;
		}

		return API_BASE_URL + label;
	}
}

// Fix a "XML Parsing Error: not well-formed" error in Firefox
//
$.ajaxSetup({
	// TODO is this still necessary?
	beforeSend: function(xhr) {
		if (xhr.overrideMimeType) {
			xhr.overrideMimeType('application/json');
		}
	}
});

function forge_headers() {

	const topFrame = window.top.topFrame;

	if (topFrame === undefined || !topFrame.hasOwnProperty('getApp')) {
		const Authorization = Cookies.get('Authorization');
		return Authorization !== null
			? { Authorization: Authorization }
			: null;
	}

	const topFrame_app = window.top.topFrame.getApp();

	return topFrame_app !== null
		? { Authorization: topFrame_app.Authorization }
		: null;
}

// Same here: Add "Content-Type: application/json"
//
jQuery['post_object'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = null;
	}

	return $.ajax({
		url: url,
		type: 'POST',
		contentType: 'application/json; charset=utf-8',
		xhrFields: { withCredentials: true },
		headers: forge_headers(),
		dataType: 'json',
		data: JSON.stringify(data),
		success: callback
	});
};

jQuery['post_plain_text'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = null;
	}

	return $.ajax({
		url: url,
		type: 'POST',
		contentType: 'application/json; charset=utf-8',
		xhrFields: { withCredentials: true },
		headers: forge_headers(),
		dataType: 'text',
		data: JSON.stringify(data),
		success: callback
	});
};

jQuery['delete_plain_text'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = undefined;
	}

	return $.ajax({
		url: url,
		type: 'DELETE',
		contentType: 'application/json; charset=utf-8',
		xhrFields: { withCredentials: true },
		headers: forge_headers(),
		dataType: 'text',
		data: data,
		success: callback
	});
};

// Same here: Add "Content-Type: application/json"
//
jQuery['get_object'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = undefined;
	}

	return $.ajax({
		url: url,
		type: 'GET',
		contentType: 'application/json; charset=utf-8',
		xhrFields: { withCredentials: true },
		headers: forge_headers(),
		dataType: 'json',
		data: data,
		success: callback
	});
};

jQuery['get_object_or_null'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = undefined;
	}

	$.get_object(url, data, callback).fail(function() {

		callback(null);
	});
}

jQuery['get_boolean'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = undefined;
	}

	$.get_object(url, data, function(json) {

		callback(json !== null && json === true);

	}).fail(function() {

		callback(false);
	});
}

// Same here: Add "Content-Type: application/json"
//
jQuery['put_object'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = undefined;
	}

	return $.ajax({
		url: url,
		type: 'PUT',
		contentType: 'application/json; charset=utf-8',
		xhrFields: { withCredentials: true },
		headers: forge_headers(),
		dataType: 'json',
		data: JSON.stringify(data),
		success: callback
	});
};

// Load the buildinfo (version, etc.) from a static file
//
loadFrame('topFrame', function() {

	$.get_object(BASE_HREF + 'buildinfo.json', function(json) {
		pre_app.buildinfo = json;
	});
});

function extractField_from_hashFragment(fieldName) {

	const hashIndex = window.location.href.indexOf('#');

	if (hashIndex === -1) {
		return null;
	}

	const slices = window.location.href.substr(hashIndex + 1).split(',');

	for (let i = 0; i < slices.length; ++i) {

		const slice = slices[i];
		
		const equalsIndex = slice.indexOf('=');

		if (equalsIndex === -1 || slice.substr(0, equalsIndex) !== fieldName) {
			continue;
		}

		return slice.substr(equalsIndex + 1);
	}

	return null;
}

function extractFlag_from_hashFragment(flagName) {

	const hashIndex = window.location.href.indexOf('#');

	if (hashIndex === -1) {
		return false;
	}

	const slices = window.location.href.substr(hashIndex + 1).split(',');

	return slices.includes(flagName);
}

function on(condition, action, after) {

	let timerId = setTimeout(function tick() {

		if (condition()) {

			clearTimeout(timerId);

			if (action.name === 'load_app') {

				const load_app = action;

				if (app) { return; }

				app = load_app();

			} else {

				action();
			}

			if (after) {
				after();
			}

		} else {

			timerId = setTimeout(tick, 100);
		}

	}, 100);
}

function global_is_standalone() {

	return window.top === window;
}

function global_API_BASE_URL() {

	return window.top.topFrame !== undefined
		? window.top.topFrame.getApp().API_BASE_URL
		: Cookies.get('API_BASE_URL');
}

function global_format_dateTimeHolder(dateTimeHolder) {

	if (dateTimeHolder === null) { return ''; }

	return dateTimeHolder.dateTime.substr(0, 10) + ' ' + dateTimeHolder.dateTime.substr(11, 8);
}

function flash_message(type, message) {

	setTimeout(function() {

		console.log('FLASH: ' + type + ': ' + message);

		const id = 'div-flashMessage-' + new Date().getTime();

		$('body').prepend('<div id="' + id + '" class="flashMessage ' + type + '"><div>'
			+ message
			+ '</div></div>');

		const div = $('#' + id);

		div.animate({ top: '-1em', opacity: 1.0 }, 200, function() {
			setTimeout(function() {
				div.animate({ top: '-5em', opacity: 0 }, 400, function() {
					div.remove();
				});
			}, 1000);
		});

	}, 100);
}

function global_frameset_href() {

	if (PAGE_ID === 'home') {
		return '../index.html';
	} else if (PAGE_ID === 'menu') {
		return '../index.html';
	} else if (PAGE_ID === 'users') {
		return '../index.html#users';
	} else if (typeof username !== 'undefined' && username !== null) {
		return '../index.html#username=' + username;
	} else {
		return '../index.html';
	}
}

function set_url_fragment(s) {

	const topFrame = window.top.topFrame;

	if (topFrame && topFrame.hasOwnProperty('getApp')) {

		window.top.location.href = BASE_HREF + 'index.html' + s;
	}
}

Vue.component('div-header', {
	template: '\
		<div id="div-header" v-if="global_is_standalone()"> \
		<table id="table-header"> \
		<tbody> \
		<tr class="apiUrl"> \
		<th> \
			<label for="text-API_BASE_URL">API_BASE_URL</label> \
		</th> \
		<td> \
			<input id="text-API_BASE_URL" type="text" class="text" disabled \
				v-bind:value="global_API_BASE_URL()"> \
		</td> \
		</tr> \
		</tbody> \
		</table> \
		<div id="div-app-title"> \
		<a v-bind:href="global_frameset_href()" target="_top"> \
			shd3/Admin \
		</a> \
		</div> <!-- end of: #div-app-title --> \
		</div> <!-- end of: #div-header --> \
	',
});
