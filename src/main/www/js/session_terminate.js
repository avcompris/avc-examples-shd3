// File: avc-examples-shd3/src/main/www/js/session_terminate.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

let session = null;
let error = null;

const userSessionId = extractField_from_hashFragment('user_session_id');

save_config(function() {

	$.get_object(get_API_BASE_URL('sessions') + '/' + userSessionId, function(json) {

		session = json;

	}).fail(function(xhr, status) {

		error = xhr.responseJSON;

		console.error(JSON.stringify(error));
	});
});

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null && session !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			error: error,
			session: session,
		},
		methods: {

			goto_session: function() {

				self.location = 'session_show.html#user_session_id=' + this.session.userSessionId + ',cancelled';
			},

			do_terminate_session: function() {

				$.post_plain_text(get_API_BASE_URL('sessions') + '/' + app.session.userSessionId + '/terminate', function(json) {

					// loadFrame('menuFrame').getApp().refresh_sessions();

					loadFrame('middleFrame', function(middleFrame) {

						if (middleFrame !== undefined) { middleFrame.getApp().do_query(); }

						self.location = 'session_show.html#user_session_id=' + app.session.userSessionId+ ',terminated';
					});

				}).fail(function(xhr, status) {

					app.error = xhr.responseJSON;

					console.error(JSON.stringify(app.error));
				});
			},
		},
		computed: {

			is_standalone: global_is_standalone,

			API_BASE_URL: global_API_BASE_URL,
		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

});
