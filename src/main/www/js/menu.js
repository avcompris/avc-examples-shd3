// File: avc-examples-shd3/src/main/www/js/menu.js

// 1. INITIALIZE THE CONTEXT

// Define: app // From framework.js // Will be populated with the Vue object
// Load: pre_app.buildinfo // From framework.js

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			users: {
				label: 'users',
				status: null,
				error: null,
				total: 0,
			},
			sessions: {
				label: 'sessions',
				status: null,
				error: null,
				total: 0,
			},
			logLines: {
				label: 'logLines',
				status: null,
				error: null,
				total: 0,
			},
		},
		methods: {

			ok_or_error: function(ok, error) {

				if (error === null) {

					return ok;

				} else {

					return error;
				}
			},

			entities_tr_class: function(label) {

				const entities = this.$data[label];

				const classes = {
					error: (entities.error !== null),
					success: (entities.error === null && entities.status === 'success'),
				};

				classes[label] = true;

				return classes;
			},

			href_entities: function(label) {

				switch (label) {
					case 'home':
						return '../middle/home.html';
					case 'users':
						return '../middle/users.html';
					case 'sessions':
						return '../middle/sessions.html';
					case 'logLines':
						return '../middle/logLines.html';
					default:
						console.error('Unknown label: ' + label);
						return label + '.html';
				}
			},
		},
		computed: {

		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

	refresh_all();
});

function refresh_all() {

	refresh_users();
	refresh_sessions();
	// refresh_logLines();
}

function refresh_users() {

	save_config(function() {

		$.get_object(get_API_BASE_URL('users') + '/', function(json) {

			app.users.total = json.total;
			app.users.status = 'success';
			app.users.error = null;

		}).fail(function(xhr, status) {

			app.users.error = xhr.status;

			console.error(JSON.stringify(app.users.error));
		});
	});
}

function refresh_sessions() {

	save_config(function() {

		$.get_object(get_API_BASE_URL('sessions') + '/', function(json) {

			app.sessions.total = json.total;
			app.sessions.status = 'success';
			app.sessions.error = null;

		}).fail(function(xhr, status) {

			app.sessions.error = xhr.status;

			console.error(JSON.stringify(app.sessions.error));
		});
	});
}
/*
function refresh_logLines() {

	save_config(function() {

		$.get_object(get_API_BASE_URL('logLines'), function(json) {

			app.logLines.total = json.total;
			app.logLines.status = 'success';
			app.logLines.error = null;

		}).fail(function(xhr, status) {

			app.logLines.error = xhr.status;

			console.error(JSON.stringify(app.logLines.error));
		});
	});
}
*/