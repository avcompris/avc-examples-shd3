// File: avc-examples-shd3/src/main/www/js/user_show.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

let user = null;
let error = null;

const username = extractField_from_hashFragment('username');
const isCancelled = extractFlag_from_hashFragment('cancelled');
const isUpdated = extractFlag_from_hashFragment('updated');
const isCreated = extractFlag_from_hashFragment('created');

save_config(function() {

	$.get_object(get_API_BASE_URL('users') + '/' + username, function(json) {

		user = json;

	}).fail(function(xhr, status) {

		error = xhr.responseJSON;

		console.error(JSON.stringify(error));
	});
});

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null && user !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			error: error,
			user: user,
		},
		methods: {

			format_dateTimeHolder: global_format_dateTimeHolder,
		},
		computed: {

			is_standalone: global_is_standalone,

			API_BASE_URL: global_API_BASE_URL,
		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

	if (isCancelled) {
		flash_message('info', 'Action has been cancelled.');
	} else if (isUpdated) {
		flash_message('success', 'The user has been updated.');
	} else if (isCreated) {
		flash_message('success', 'The user has been created.');
	}
});

function update_hash() {

	const username = extractField_from_hashFragment('username');

	save_config(function() {

		$.get_object(get_API_BASE_URL('users') + '/' + username, function(json) {

			user = json;

			if (app) { app.user = json; }

		}).fail(function(xhr, status) {

			error = xhr.responseJSON;

			console.error(JSON.stringify(error));

			if (app) { app.error = xhr.responseJSON; }
		});
	});
}