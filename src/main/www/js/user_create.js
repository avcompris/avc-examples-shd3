// File: avc-examples-shd3/src/main/www/js/user_create.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

let error = null;

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			error: error,
			username: null,
			role: null,
			password: null,
			enabled: true,
			roles: ROLES,
		},
		methods: {

			do_cancel: function() {

				if (this.is_standalone) {

					self.location = 'users.html#cancelled';
				
				} else {

					self.location = 'empty.html#cancelled';
				}
			},

			do_create_user: function() {

				$.post_object(get_API_BASE_URL('users') + '/' + app.username, {
					role: app.role,
					password: app.password,
					enabled: app.enabled,
				}, function(json) {

					loadFrame('leftFrame', function(leftFrame) {

						if (leftFrame !== undefined) { leftFrame.refresh_users(); }

						loadFrame('middleFrame', function(middleFrame) {

							if (middleFrame !== undefined) { middleFrame.getApp().do_query(); }

							self.location = 'user_show.html#username=' + app.username + ',created';
						});
					});

				}).fail(function(xhr, status) {

					app.error = xhr.responseJSON;

					console.error(JSON.stringify(app.error));
				});
			},
		},
		computed: {

			is_standalone: global_is_standalone,

			API_BASE_URL: global_API_BASE_URL,
		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

});
