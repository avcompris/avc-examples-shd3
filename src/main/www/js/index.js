// File: avc-examples-shd3/src/main/www/js/index.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

const username = extractField_from_hashFragment('username');
const logLineId = extractField_from_hashFragment('logLineId');

const isEdit = extractFlag_from_hashFragment('edit');
const isUsers = extractFlag_from_hashFragment('users');
const isWorkers = extractFlag_from_hashFragment('workers');

const isUserCreate = extractFlag_from_hashFragment('user_create');

function update_frame_locations() {

	if (isUsers) {

		window.middleFrame.location = './frames/middle/users.html';

	} else if (isWorkers) {

		window.middleFrame.location = './frames/middle/workers.html';

	} else if (username !== null) {

		window.middleFrame.location = './frames/middle/users.html';

		if (isEdit) {
			window.rightFrame.location = './frames/right/user_edit.html#username=' + username;
		} else {
			window.rightFrame.location = './frames/right/user_show.html#username=' + username;
		}

	} else if (logLineId !== null) {

		window.middleFrame.location = './frames/middle/workers.html';

		window.rightFrame.location = './frames/right/workerLogLine_show.html#logLineId=' + logLineId;

	} else if (isUserCreate) {

		window.middleFrame.location = './frames/middle/users.html';
		window.rightFrame.location = './frames/right/user_create.html';
	}
}
