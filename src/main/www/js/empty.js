// File: avc-examples-shd3/src/main/www/js/empty.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

const isCancelled = extractFlag_from_hashFragment('cancelled');
const isDeleted = extractFlag_from_hashFragment('deleted');

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			error: null,
		},
		methods: {

		},
		computed: {

		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

	if (isCancelled) {
		flash_message('info', 'Action has been cancelled.');
	} else {
		flash_message('info', 'Item has been deleted.');
	}
});
