// File: avc-examples-shd3/src/main/www/js/user_delete.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

let user = null;
let error = null;

const username = extractField_from_hashFragment('username');

save_config(function() {

	$.get_object(get_API_BASE_URL('users') + '/' + username, function(json) {

		user = json;

	}).fail(function(xhr, status) {

		error = xhr.responseJSON;

		console.error(JSON.stringify(error));
	});
});

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null && user !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			error: error,
			user: user,
		},
		methods: {

			goto_user: function() {

				self.location = 'user_show.html#username=' + this.user.username + ',cancelled';
			},

			do_delete_user: function() {

				$.delete_plain_text(get_API_BASE_URL('users') + '/' + app.user.username, function(json) {

					// loadFrame('menuFrame').getApp().refresh_users();

					loadFrame('leftFrame', function(leftFrame) {

						if (leftFrame !== undefined) { leftFrame.refresh_users(); }

						loadFrame('middleFrame', function(middleFrame) {

							if (middleFrame !== undefined) { middleFrame.getApp().do_query(); }

							self.location = 'empty.html#deleted';
						});
					});

				}).fail(function(xhr, status) {

					app.error = xhr.responseJSON;

					console.error(JSON.stringify(app.error));
				});
			},
		},
		computed: {

			is_standalone: global_is_standalone,

			API_BASE_URL: global_API_BASE_URL,
		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

});
