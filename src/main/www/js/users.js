// File: avc-examples-shd3/src/main/www/js/users.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

const username = extractField_from_hashFragment('username');
const isCancelled = extractFlag_from_hashFragment('cancelled');

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			error: null,
			users: null,
			total: 0,
			page: {
				q: null,
				sort: null,
				start: 0,
				limit: 10,
			},
		},
		methods: {

			format_dateTimeHolder: global_format_dateTimeHolder,

			tr_result_class: function(index) {
				return { odd: index % 2 === 0 };
			},

			href_sort_by: function(fieldName) {
				return '#';
			},
			href_user: function(username) {
				return '../right/user_show.html#username=' + username;
			},

			goto_first_page: function() {
				this.page.start = 0;
				this.do_query();
			},
			goto_prev_page: function() {
				this.page.start = Math.max(0, this.users.start - this.users.limit);
				this.do_query();
			},
			goto_next_page: function() {
				this.page.start = this.users.start + this.users.limit;
				this.do_query();
			},
			goto_last_page: function() {
				while (this.page.start + this.users.limit < this.users.total) {
					this.page.start += this.users.limit;
				}
				this.do_query();
			},
			sort_by: function(fieldName) {
				this.page.start = 0;
				this.page.sort = (this.page.sort === fieldName) ? ('-' + fieldName) : fieldName;
				this.do_query();
			},

			format_resultCount: function(resultCount, total) {
				if (resultCount === 0 && total === 0) {
					return 'There are no users in the database.';
				} else if (resultCount === 0) {
					return 'Your query returned no user, out of ' + total + ' in the database.';
				} else if (resultCount === 1 && total === 1) {
					return 'Your query returned the only user in the database.';
				} else if (resultCount === 1) {
					return 'Your query returned one user, out of ' + total + ' in the database.';
				} else if (resultCount !== total) {
					return 'Your query returned ' + resultCount + ' users, out of ' + total + ' in the database.';
				} else {
					return 'Your query returned all ' + total + ' users from the database.';
				}
			},

			do_query: function() {

				this.error = null;

				save_config(function() {

					$.get_object(get_API_BASE_URL('users') + '?limit=0', function(json) {

						app.total = json.total;
					});

					$.post_object(get_API_BASE_URL('users'), {
						q: app.page.q,
						sort: app.page.sort,
						start: app.page.start,
						limit: app.page.limit,
					}, function(json) {

						app.users = json;
						// app.page.q = app.users.q;
						// app.page.sort = app.users.sort;
						app.page.start = app.users.start;
						app.page.limit = app.users.limit;

					}).fail(function(xhr, status) {

						app.error = xhr.responseJSON;

						console.error(JSON.stringify(app.error));
					});
				});
			},
		},
		computed: {

			is_standalone: global_is_standalone,

			API_BASE_URL: global_API_BASE_URL,

			has_prev_page: function() {
				return this.users !== null && this.users.start > 0;
			},
			href_first_page: function() {
				return '#';
			},
			href_prev_page: function() {
				return '#';
			},
			has_next_page: function() {
				return this.users !== null && this.users.start + this.users.limit < this.users.total;
			},
			href_last_page: function() {
				return '#';
			},
			href_next_page: function() {
				return '#';
			},
			page_number: function() {
				return this.users === null
					? 0
					: 1 + Math.floor((this.users.start + this.users.limit - 1) / this.users.limit);
			},
			page_count: function() {
				return this.users === null
					? 0
					: Math.floor((this.users.total + this.users.limit - 1) / this.users.limit);
			},
		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

	app.do_query();

	if (isCancelled) {
		flash_message('info', 'Action has been cancelled.');
	}
});
