// File: avc-examples-shd3/src/main/www/js/sessions.js

// 1. INITIALIZE THE CONTEXT

// var buildinfo // From script.js

const userSessionId = extractField_from_hashFragment('user_session_id');

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			error: null,
			sessions: null,
			total: 0,
			page: {
				q: null,
				sort: null,
				start: 0,
				limit: 10,
			},
		},
		methods: {

			format_dateTimeHolder: global_format_dateTimeHolder,

			tr_result_class: function(index) {
				return { odd: index % 2 === 0 };
			},

			href_sort_by: function(fieldName) {
				return '#';
			},
			href_session: function(userSessionId) {
				return '../right/session_show.html#user_session_id=' + userSessionId;
			},

			goto_first_page: function() {
				this.page.start = 0;
				this.do_query();
			},
			goto_prev_page: function() {
				this.page.start = Math.max(0, this.sessions.start - this.sessions.limit);
				this.do_query();
			},
			goto_next_page: function() {
				this.page.start = this.sessions.start + this.sessions.limit;
				this.do_query();
			},
			goto_last_page: function() {
				while (this.page.start + this.sessions.limit < this.sessions.total) {
					this.page.start += this.sessions.limit;
				}
				this.do_query();
			},
			sort_by: function(fieldName) {
				this.page.start = 0;
				this.page.sort = (this.page.sort === fieldName) ? ('-' + fieldName) : fieldName;
				this.do_query();
			},

			format_resultCount: function(resultCount, total) {
				if (resultCount === 0 && total === 0) {
					return 'There are no sessions in the database.';
				} else if (resultCount === 0) {
					return 'Your query returned no session, out of ' + total + ' in the database.';
				} else if (resultCount === 1 && total === 1) {
					return 'Your query returned the only session in the database.';
				} else if (resultCount === 1) {
					return 'Your query returned one session, out of ' + total + ' in the database.';
				} else if (resultCount !== total) {
					return 'Your query returned ' + resultCount + ' sessions, out of ' + total + ' in the database.';
				} else {
					return 'Your query returned all ' + total + ' sessions from the database.';
				}
			},

			do_query: function() {

				this.error = null;

				save_config(function() {

					$.get_object(get_API_BASE_URL('sessions') + '?limit=0', function(json) {

						app.total = json.total;
					});

					$.post_object(get_API_BASE_URL('sessions'), {
						q: app.page.q,
						sort: app.page.sort,
						start: app.page.start,
						limit: app.page.limit,
					}, function(json) {

						app.sessions = json;
						// app.page.q = app.sessions.q;
						// app.page.sort = app.sessions.sort;
						app.page.start = app.sessions.start;
						app.page.limit = app.sessions.limit;

					}).fail(function(xhr, status) {

						app.error = xhr.responseJSON;

						console.error(JSON.stringify(app.error));
					});
				});
			},
		},
		computed: {

			is_standalone: global_is_standalone,

			API_BASE_URL: global_API_BASE_URL,

			has_prev_page: function() {
				return this.sessions !== null && this.sessions.start > 0;
			},
			href_first_page: function() {
				return '#';
			},
			href_prev_page: function() {
				return '#';
			},
			has_next_page: function() {
				return this.sessions !== null && this.sessions.start + this.sessions.limit < this.sessions.total;
			},
			href_last_page: function() {
				return '#';
			},
			href_next_page: function() {
				return '#';
			},
			page_number: function() {
				return this.sessions === null
					? 0
					: 1 + Math.floor((this.sessions.start + this.sessions.limit - 1) / this.sessions.limit);
			},
			page_count: function() {
				return this.sessions === null
					? 0
					: Math.floor((this.sessions.total + this.sessions.limit - 1) / this.sessions.limit);
			},
		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

	app.do_query();
});
